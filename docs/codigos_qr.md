# Codigos QR

## Códigos QR de los dispositivos

Los mismos serán impresos en los dispositivos para poder indentificarlos rápidamente con celulares, tablets y afines.

El código QR debe contener un JSON de manera de poder agregar datos a futuro. Actualmente el mismo se define como:

```json
{
  "mac":"<mac address>",
  "hw_ver" : "<string>",
  "firm_ver" : "x.y.z",
}
```

Y deberá implementarse sin espacios ni saltos de línea.

- **mac** en minúsculas sin ':'.
- **hw_ver** String libre pero sin espacios.
- **firm_ver** Versión de firmware de fábrica siguiendo [Semantic versioning 2.0](https://semver.org/).

Ejemplo:

    {"mac":"001122334455","hw_ver":"202005_A","firm_ver":"0.0.1"}

Un ejemplo con dot (pixel) = 7, nivel de corrección máximo con [qrencode](https://fukuchi.org/works/qrencode/index.html.en) es:

    qrencode -s7 -l H -o mac.png '{"mac":"001122334455","hw_ver":"202005_A","firm_ver":"0.0.1"}'

![Ejemplo de código Qr](images/mac.png)

<a name="qr_en_monitor"></a>
## Códigos QR en el monitor principal

El monitor principal (corriendo en el server) podrá opcionalmente mostrar en pantalla un código QR de una internación en particular en caso de que el usuario de un monitor móvil quiera ver los datos sin necesidad de acercarse al dispositivo. En este caso el código QR podrá proporcionar solamente el campo *mac*.
