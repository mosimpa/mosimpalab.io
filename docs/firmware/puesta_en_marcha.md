# Puesta en marcha

Mientras se encuentre en desarrollo inicial corresponde al [issue 10](https://gitlab.com/mosimpa/mosimpa.gitlab.io/-/issues/10) en el repositorio de este sitio en GitLab.

# Flujo del software que controla al test rig

![Flujo del programa](test_rig_flow.png)

Fuente: [test_rig_flow.uxf](test_rig_flow.uxf). Se modifica con umlet o [umletino](https://www.umletino.com/umletino.html).	
