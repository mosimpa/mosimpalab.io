# Monitoreo simplificado de pacientes en situación de internación masiva (MoSimPa) - Propuestas de desarrollo

## Conectividad

Ver [la página al respecto](propuesta/conectividad.md).

## Datos a transmitir

Ver [la página al respecto](datos_a_transmitir.md).

## ESP32

Ver la página del [ESP32](propuesta/esp32.md).

## Sugerencia: usar MQTT

Página: [MQTT](propuesta/mqtt.md)

Motivos:

- Permite generar un bus virtual
- Ampliamente probado
- Escala a miles de dispositivos muy fácil
- Corre con pocos recursos (probé 100 conexiones simultáneas con 138 bytes de
  payload una vez por segundo sin problemas en una RPi 3B+)

Links:

- <https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/>

### Sugerencia: usar micropython.

Motivos:

- Hay mas programadores (humanos) de python disponibles que programadores de C.
- Tiempo de código generalmente mas rápido.

Links:

- <https://docs.micropython.org/en/latest/esp32/tutorial/intro.html>

**DESCARTADO:** el MAX32664D tiene una interrupción y, si bien es manejable desde micropython, puede no ser lo mas adecuado en términos de tiempos y recursos.

### Sugerencia: usar el SDK de Arduino para el ESP32

- También hay mucha gente que usa el SDK.
- Manejaríamos mejor interrupciones y memoria.


### Sugerencia: usar CBOR para encapsular los datos

- Se puede disminuir considerablemente el ancho de banda (comparado con JSON puro)
- Al igual que JSON es schema-less, lo que permite mayor flexibilidad ante cambios.
