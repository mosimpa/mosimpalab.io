# Generación del SO

Si bien usamos Raspbian como base para el sistema operativo se hace necesario modificarlo para nuestro uso:

- Quitar software que es irrelevante para nuestro propósito, como ser juegos, editores de imágenes, herramientas y documentación de desarrollo, web browsers, etc.
- Tener la posibilidad de dejar el sistema lo mas preconfigurado posible, de manera que los ajustes finales por equipo sean lo mas mínimos posible.

La generación del SO se divide en dos partes principales:

1. Generar la imagen "master" que luego será copiada en las tarjetas de memoria µSD para las Raspberry Pi.
2. Bootear la Pi con la tarjeta y proceder a instalar paquetes que no pueden ser instalados en la imagen master por diferentes razones. En el proceso se verifica que el sistema ande y se configuran contraseñas específicas para cada sistema.

# Creación de la imagen master

Para la creación de la imagen master vamos a modificar los scripts que se utilizan para generar Raspbian disponibles en el repositorio [pi-gen](https://github.com/RPi-Distro/pi-gen) en Github. Para mantener los archivos en el mismo servidor tenemos una [copia en Gitlab](https://gitlab.com/mosimpa/pi-gen).

La rama principal para nuestro proyecto es mosimpa-server. La rama master la reservamos para el repositorio original.

Para entender como se trabaja con este repositorio se aconseja primero leer el [README](https://gitlab.com/mosimpa/pi-gen/-/blob/mosimpa-server/README.md) del mismo.

Para modificar el árbol de ejecución se decidió:

- Quitar de las stages que provee Raspbian aquellas aplicaciones que no queremos sean parte de la imagen master.
- Para agregar software específico se crean stages complementarias de la forma stage*n*_mosimpa.
- Listar el orden de stages en mosimpa.config.

Actualmente la última stage de nuestra imagen es stage3_mosimpa.

Luego de correr el script build.sh (o build-docker.sh) la imagen queda lista en deploy:

    deploy/YYYY-MM-DD-mosimpa-server-mosimpaserver.img

Para cargar la imagen en una tarjeta µSD:

```bash
sudo cat deploy/YYYY-MM-DD-mosimpa-server-mosimpaserver.img > /dev/sdx
```

Donde sdx es el nodo **raíz** de la tarjeta µSD y no una partición (es decir no sdx1). Se sugiere utilizar lsblk para corroborar en que nodo se encuentra la tarjeta.

# Primer booteo y configuración

Durante el primer booteo la imagen se ampliará automáticamente para ocupar el total del tamaño de la tarjeta µSD. Posteriormente rebooteará. En este punto es necesario llevar a cabo algunos pasos, actualmente manuales. Parte de los scripts necesarios se encuentran en el directorio post-boot.

- Reinstalar raspberrypi-ui-mods: por algún motivo que desconozco es necesario reinstalar este paquete para lograr que la integración funcione.

```bash
sudo apt reinstall raspberrypi-ui-mods
```

- Precargar la configuración de debconf.

```bash
sed -i 's/@DATAKEEPER_APP_PASS@/ThePassword/g' 00-debconf
debconf-set-selections < 00-debconf
DEBIAN_FRONTEND=noninteractive apt install -y mosimpa-datakeeper
sed -i 's/youshouldreallychangeme/ThePassword/g' /etc/xdg/MoSimPa/datakeeper.conf
sed -i 's/youshouldreallychangeme/ThePassword/g' /home/oxigeno/.config/MoSimPa/abm.conf
```

En este punto es necesario reiniciar el sistema o el servicio, en éste último caso ejecutando:

```bash
systemctl restart mosimpa-datakeeper.service
```

## Certificados

Se usa por defecto los certificados snakeoil para apache2 y mosquitto. Hay que exportar el certificado */etc/ssl/certs/ssl-cert-snakeoil.pem* para los dispositivos y estaciones de trabajo que lo requieran. Se recomienda renombrar el archivo a *mosimpa-server-XXXXXXXXXXXX.crt* donde XXXXXXXXXXXX es la MAC address del dispositivo en minúsculas.

En caso de que el monitor se corra desde una máquina externa, si el sistema host es un Debian, se puede instalar el certificado en */usr/local/share/ca-certificates/* y luego correr:

```bash
sudo update-ca-certificates
```

## Usuario(s) para acceder a los reportes

Generación del usuario para obtener reportes. Se debe correr:

```bash
sudo htpasswd -c /etc/mosimpa-datakeeper/htpasswd theuser
# Enter the desired password.
```

En caso de querer agregar mas usuarios mirar la documentación de htpasswd.

# Reducción de la imagen

Si bien es posible que no sea necesario se deja documentado el proceso.

El procedimiento es básicamente el presentado en ["Create a custom Raspbian OS image for production"](https://medium.com/platformer-blog/creating-a-custom-raspbian-os-image-for-production-3fcb43ff3630). Las diferencias son los parámetros al usar dd.

## Copiar los contenidos de la tarjeta a una imagen local

Donde /dev/mmcblk0 represnta al device node del lector de tarjeta. Puede variar de PC en PC, usar lsblk para encontrar el correcto.

Todos los procedimientos pueden tardar bastante tiempo.

    dd if=/dev/mmcblk0 of=mosimpa_big.img bs=4M status=progress conv=fsync

## Achicar la imagen

```
wget  https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh
chmod 755 pishrink.sh
sudo ./pishrink.sh mosimpa_big.img mosimpa.img
```

## Comprimir la imagen resultante

    xz -vv -9 mosimpa.img
