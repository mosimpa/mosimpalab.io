# MQTT

## Links de interés

- [Introduction to MQTT security mechanisms](http://www.steves-internet-guide.com/mqtt-security-mechanisms/).

---
# Broker en Raspbian o Debian

## Instalación

En una consola:

    sudo apt install mosquitto
   
---
## Configuración

- Vamos a trabajar en tres etapas, a medida que se lleven a cabo se va a ir agregando la configuración a tal fin. Luego queda definir que tipo de instalación se va a llevar a cabo.

1. Sin ningún tipo de seguridad
2. Con usuario y contraseña
3. Usando TLS
4. Combinando métodos anteriores.

---
### Configuración en común

Crear el archivo /etc/mosquitto/conf.d/mosimpa.conf y agregar:

```
log_timestamp true
```

---
### Configuración sin ningún tipo de seguridad

#### Pros

- Simple de hacer andar
- No requiere ningún tipo de algoritmo extra en el dispositivo concentrador.

#### Contras

- Cualquier dispositivo en la red puede enviar datos, incluyendo malignamente.
- Cualquier dispositivo en la red puede espiar los datos.

#### Configuración

Agregar a /etc/mosquitto/conf.d/mosimpa.conf:

```
allow_anonymous true
```

---
### Configuración con usuario y contraseña

#### Pros

- Simple de hacer andar.
- El usuario y contraseña puede o no ser compartido.
- Se puede utilizar para restringir el acceso a tópicos.

#### Contras

- El usuario y contraseña viajan en texto plano por la red, por ende es posible obtenerlos sniffeando los paquetes de la red.
- Cualquier dispositivo en la red puede espiar los datos.

#### Configuración

Agregar a /etc/mosquitto/conf.d/mosimpa.conf:

```
allow_anonymous false
password_file /etc/mosquitto/pwfile
```

Y luego crear los usuarios con sus contraseñas siguiendo [las siguientes instrucciones](http://www.steves-internet-guide.com/mqtt-username-contraseña-example/).

---
### Configuración usando TLS

#### Pros

- Toda la información que se trafica en la red es encriptada, ya que es a nivel TCP/IP. Se puede pensar en un túnel encriptado.

#### Contras

- Requiere mas capacidades de los dispositivos.

#### Configuración

A hacer si se piensa llevar a cabo esta metodología.


### Configuración con combinación de varios métodos

Es posible pensar en dos redes: una red en particular para los dispositivos sensores y otra para aquellos externos como visualizadores externos (tablets, celulares y monitores externos).

La red de los dispositivos sensores puede ser exclusiva para ellos y utilizar la metodología anónima o con usuario y password.

Por otro lado la red de visualizadores/consumidores puede usar certificados o un juego de usuario/password que solo permita la visualización de ciertos datos. Esto posiblemente requeriría un cliente que pase datos de un tópico a otro.

---
**IDEA:** es posible hacer una GUI para automatizar el proceso y utilizar *moquitto_passwd -U*

