# ESP32

- Posee dos controladores I²C, podemos usar uno por sensor.
- Posee WiFi con soporte TLS.

Se puede poner en modo AP y cliente (posiblemente al mismo tiempo). Se podría poner en modo AP y poner un [web server sencillo](https://lastminuteengineers.com/creating-esp32-web-server-arduino-ide/) para poder configurar la red a la que debe conectarse.

Todas las características necesarias parecen estar disponibles en micropython, incluyendo [MQTT](https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/).

# Diagramas de flujo

## Diagrama de fujo principal

[Fuente](esp32/esp32_main_flow.dia).

![Flujo principal](esp32/esp32_main_flow.png "Diagrama de flujo principal")


## Procedimiento para establecer SSID, contraseña y datos del broker MQTT

[Fuente](esp32/connection_data.dia).

![SSID, password, datos broker MQTT](esp32/connection_data.png "Procedimiento para establecer SSID, contraseña y datos del broker MQTT")
