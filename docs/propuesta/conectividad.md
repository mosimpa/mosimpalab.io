
# Conectividad

![Esquema de datos](conectividad/esquema_conexiones.png)
[Fuente](conectividad/esquema_conexiones.odg)

La comunicación se realiza utilizando tres protocolos:

- [MQTT](http://mqtt.org/): un protocolo de conectividad machine-to-machine (M2M) muy liviano y de alta escalabilidad, bajo footprint de código y ancho de banda.
- [SQL](https://www.postgresql.org/): en particular el protocolo de red de PostgreSql, ya que la base de datos se implementa con ese servidor.
- HTTP o HTTP: el protocolo HTTP o su variante con TLS para manejar Alta, Bajas y Modificaciones (**ABM**).


Los servicios marcados dentro de la Raspberry Pi / server pueden o no estar en conjunto en el mismo, dependiendo de la escalabilidad que se le pretenda dar al sistema.

Este esquema permite que:

- Todos los dispositivos puedan enviar datos al broker fácilmente (el protocolo MQTT se ha implementado en PICs de poca potencia).
- Todas las aplicaciones que consumen los datos puedan acceder a los mismos en tiempo real.
- Las aplicaciones móviles no necesitan disponer de base de datos, pueden requerirlos al datakeeper u otra aplicación afín.

## Componentes

- **Dn** cada uno de los dispositivos MoSimPa.
- **Broker MQTT** servidor de mensajería MQTT. Actualmente utilizamos [mosquitto](https://mosquitto.org/).
- **datakeeper** es la aplicación que interfasea entre los dispositivos, los monitores y la base de datos. Su código fuente se encuentra en [este repositorio](https://gitlab.com/mosimpa/datakeeper/).
- **Monitor general** es laplicación utilizada para monitoreo de los pacientes en forma masiva desde la Pi o alguna otra máquina con Linux. Su código fuente se encuentra en [este repositorio](https://gitlab.com/mosimpa/monitor/).
- **Base de datos** guarda los datos de pacientes, ubicaciones, internaciones y valores históricos entre otros. Utilizamos [PostgreSQL](https://www.postgresql.org/).
- **Servidor web** a futuro correrá la aplicación de **A**lta, **B**aja y **M**odificación (ABM) web.
- **ABM** aplicación de aplicación de **A**lta, **B**aja y **M**odificación. Actualmente desarrollada en C++ [aquí](https://gitlab.com/mosimpa/abm/), a futuro se reemplazará por una aplicación web. Su funcionalidad se especifica [aquí](especificacion_abm.md).
- **Tablets/celulares** los mismos podrán acceder a los datos a través del [monitor para Android](https://gitlab.com/mosimpa/mosimpa-android).

## Extras (para considerar a posteriori):

- Se puede hacer autenticación de dispositivos directamente desde el broker MQTT.
- Se pueden hacer conexiones encriptadas.
- Se pueden agregar tantas aplicaciones de monitoreo como sea deseado (en éste caso sin replicar el broker)
- Se puede realizar autenticación de usuarios.
