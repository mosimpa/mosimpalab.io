# Especificación ABM

Ésta es una especificación del juego de funcionalidades mínimas que debe tener un ABM/[CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete). La intención es que sea una aplicación administrativa, es decir, que no se utilice para el monitoreo de pacientes.

La base de datos se crea con los [scripts del repositorio de datakeeper](https://gitlab.com/mosimpa/datakeeper/-/tree/master/sql).

![ABM done with Qt](images/abm_qt.png)
Imagen del ABM realizado con [Qt](https://www.qt.io/developers).

Un ABM debe poder:

- Listar los dispositivos MoSimPa conocidos por la base de datos.
- Debe ser capaz de realizar la creación, lectura, actualización y borrado de ubicaciones, pacientes e internaciones.

## Fechas y horas

La base de datos utiliza todas las fechas y horas con time zones, es decir, usando [timestamptz aka 'timestamp with time zone'](https://www.postgresql.org/docs/11/datatype-datetime.html). Es responsabilidad del ABM de siempre pasar la zona horaria del cliente en las fechas y horas.

## Dispositivos

Definidos en la [tabla devices](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_tables.sql).

Los dispositivos son agregados automáticamente por [datakeeper](https://gitlab.com/mosimpa/datakeeper/). La aplicación deberá ser capaz de mostrar:

- La MAC, que es utilizada como identificador único.
- La fecha y hora en la que se vió por primera vez en el sistema.
- La fecha y hora de la última vez que se obtuvieron datos del mismo. Se actualiza automáticamente cada vez que el dispositivo publica un mensaje en el topic [devices/info](https://mosimpa.gitlab.io/datos_a_transmitir/#topic-devicesinfo).
- (Opcional) Nivel de batería. Actualmente no se muestra porque no se conoce aún la curva de la misma.

## Ubicaciones

Definidas en la [tabla locations](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_tables.sql).

CRUD completo para:

- ID: generado automáticamente por la base de datos.
- Tipo: definido por el enum location_type.
- Descripción.

Observar las restricciones en la definición de la tabla.

## Pacientes

Definidos en la [tabla patients](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_tables.sql).

CRUD completo para:

- ID: generado automáticamente por la base de datos.
- Nombre
- Apellido
- Edad
- Género
- DNI
- Comentarios

### Observar

- Las restricciones en la definición de la tabla.
- Los campos name y surname deben ser [simplificados](https://doc.qt.io/qt-5/qstring.html#simplified), es decir, sin espacios extra.

## Internaciones

Definidas en la [tabla internments](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_tables.sql).

CRUD completo para:

- ID: generado automáticamente por la base de datos.
- Fecha de inicio
- Fecha de finalización: es importante notar que la misma se utilizará después para saber que dispositivos y ubicaciones están disponibles: aquellos que o no figuran en esta tabla o figuran pero esta fecha no es NULL.
- ID del paciente
- Nombre y apellido del paciente.
- Ubicación
- Dispositivo asociado

El resto de los datos se reservan a los monitores.

## Stored procedures

La base de datos cuenta con varios stored procedures para tratar de mantener la coherencia de los datos. Los mismos se encuentran definidos [aquí](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_stored_procedures.sql).

Existe un stored procedure para agregar pacientes, ubicaciones e internaciones. El ABM debería hacer uso de los mismos.

## Vistas

La base de datos cuenta con varias vistas que resultan convenientes para el ABM. Las mismas se encuentran definidas [aquí](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_views.sql). Por ejemplo merged_name_surname_patients muestra el nombre y apellidos de un paciente como si fuese un solo campo, mientras que available_locations aplica los filtros necesarios para poder listar las ubicaciones disponibles.