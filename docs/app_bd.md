# Aplicación de base de datos

## Requerimientos

- Debe poder registrar a los pacientes y sus datos.
- Debe poder registrar a los dispositivos.
- Debe poder asociar pacientes con dispositivos de forma biunívoca:
    - no es posible que un mismo dispositivo esté asociado a dos pacientes al mismo tiempo.
    - ¿es posible que un paciente esté asociado a dos dispositivos al mismo tiempo?
- Debe poder guardar los datos generados por los sensores de los dispositivos (de ahora en mas, lecturas).
- Debe tener la capacidad de eliminar lecturas luego de tantas horas.
    - De forma manual.
    - De forma automática.
- Debe poder proveer a los monitores de la información histórica necesaria (los valores actuales se irán tomando de la misma fuente).
- Debe poder generar alarmas a consumir por los monitores.
- Debe poder configurar los niveles de alarma por dispositivo.
- Debe poder arbitrar el acceso de escritura de los monitores, de manera de que no se produzcan cargas dobles ni alteraciones indeseadas en los datos (atomicidad).
- Debe poder guardar cambios sin sobre escribir información previa. La única excepción sea quizás la tensión de batería del dispositivo.
- Debe poder tener en cuenta cada reinicio de un dispositivo para mantener la linea temporal.

## Requerimientos opcionales

Éstos son requerimientos que no es necesario que estén pero pueden ayudar a la calidad final del software.

- Registrar a los usuarios
- Enviar información de niveles de alarma a los dispositivos para que actualicen su tasa de refesco de modo acorde. Ejemplo: si los parámetros son normales se pueden enviar datos a menor tasa de refresco, pero si se corren se podrán enviar a medida que se generan.
- Debe registrar a los usuarios que hacen los cambios y fecha y hora exactas.

---

## Datos de los pacientes

- Nombre
- Apellido
- Sexo
- Edad
- ¿DNI?
- Cama

## Datos de los dispositivos

- MAC
- Tensión de batería

