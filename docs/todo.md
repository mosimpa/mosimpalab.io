# To Do

---
# Especificaciones

# Protocolo de red

## Seguridad

- Usar SSL. Aplica a esp32, broker MQTT y monitores.
- Autenticación, idem arriba.


## Payload

- Queda definir si se transmite o no la frecuencia respiratoria y que tipo de datos transmite.
- Usar un formato comprimido para el payload (¿CBOR?)
- Envío de datos A, B y C al ESP32.

---
# Integración

Hay que ver como testear la integración completa de las partes.

---
# Documentación

- Documentar ABM, Datakeeper y Monitor y su interrelación.

---
# Generación de imágenes

- Automatizar.

---
# Software

## General

- Hacer lib con el código común entre el resto del software.
- Integrar RTC en la Pi.

## datakeeper

Ver [la página de Issues de Gitlab](https://gitlab.com/mosimpa/datakeeper/-/issues).

## abm

- ¿Seguimos desarrollo?
- Manejar fechas para cambio entre UTC y local.
- Usar los QR para asociar el dispositivo mas rápido
- Especificación de los datos A, B y C para el cálculo de SpO2.
- ...mucho

## abm web

- Conseguir desarrollador: en proceso.

## Monitor

Ver [la página de Issues de Gitlab](https://gitlab.com/mosimpa/monitor/-/issues).

## ESP32

- Posibilidad de recibir y aplicar datos A, B y C requeridos para el cálculo de SpO2.

## Generación de etiquetas con QR para los dispositivos

Esto puede o no ser un programa dedicado, para la prueba inicial se puede hacer con gLabels directamente.

---
# Interacción con el usuario

- Definir como se va a llevar a cabo la calibración del algoritmo BPT para cada usuario
     - ¿Guardamos la calibración en el ESP32?
     - ¿Se realiza a través de un monitor? ¿Con el ABM? ¿ambos?
         - Requiere medir presión sanguínea tres veces, por lo que el paciente requiere estar quieto cierto tiempo.