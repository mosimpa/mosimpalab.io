# Proceso de release de software para repositorios usando git flow

Algunos de los repositorios de MoSimPa se trabajan utilizando [git flow](https://nvie.com/posts/a-successful-git-branching-model/). El presente documento establece el procedimiento para realizar una release de una versión de software.


# Acciones previas a iniciar una release

- Verificar que todos los cambios tengan una entrada acorde en *debian/changelog*.
- Verificar que los tests unitarios pasen.
- Verificar que no queden cambios en el repositoro sin committear.
- Limpiar el repositorio usando

```bash
$ git clean -xdff
```


# Proceso de release

La rama del release llevará por nombre la versión del mismo. Generalmente esto se documenta en *CMakeLists.txt* y *debian/changelog*

En CMakeLists.txt:

```cmake
# Application version.
set(MONITOR_MAJOR_VERSION "0")
set(MONITOR_MINOR_VERSION "0")
set(MONITOR_PATCH_VERSION "36")
# Release/beta candidate, keep empty on official versions.
set(MONITOR_RC_VERSION "~")
```

En debian/changelog:

```bash
$ head debian/changelog
mosimpa-monitor (0.0.36-0r0) UNRELEASED; urgency=medium


 -- Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>  Mon, 21 Sep 2020 10:14:44 -0300

```

De ambos archivos podemos ver que la próxima versión estable será la 0.0.36. Iniciamos entonces la rama de release con:

```bash
$ git flow release start 0.0.36
```

A continuación modificamos CMakeList.txt dejando vacía la definición de MONITOR_RC_VERSION.

```cmake
# Application version.
set(MONITOR_MAJOR_VERSION "0")
set(MONITOR_MINOR_VERSION "0")
set(MONITOR_PATCH_VERSION "36")
# Release/beta candidate, keep empty on official versions.
set(MONITOR_RC_VERSION "")
```

Luego realizamos el cierre del archivo *debian/changelog*:

```bash
$ DEBEMAIL="mosimpa@example.com" DEBFULLNAME="A MoSimPa developer" dch -r
```

Se abrirá un editor de texto y se reemplza la versión 0.0.36-0r0 por 0.0.36-1. Es decir, se modifica la revisión de empaquetado.

En este momento se copian las entradas del último changelog para usar posteriormente al crear la tag del repositorio.

Se realiza un commit con el título "Release version 0.0.36-1". Notar que es la versión tag + versión Debian.

Se procede luego a cerrar el release:

```bash
$ git flow release finish 0.0.36
```

Primer commit: se debe dejar el mensaje de commit propuesto por la herramienta.
Segundo commit (tag): Se debe usar "Version 0.0.36" para el título y se debe pegar las entradas del changelog como explicación del mismo:

```
Merge tag '0.0.36' into develop
  
Version 0.0.36.

* A change.
* Another change.
```

Tercer commit: se hace merge de los cambios en la rama develop, usando lo propuesto por la herramienta.

# Post release

Generar una nueva versión de desarrollo. En *CMakeLists.txt*:
- Incrementar MONITOR_*_VERSION de la manera necesaria.
- Setear MONITOR_RC_VERSION a ~

Abrir una nueva versión en el changelog:

```bash
DEBEMAIL=perezmeyer@gmail.com DEBFULLNAME="A MoSimPa developer" dch -v 0.0.37-0r0
```

Notar la revisión Debian 0r0, esto permite generar paquetes intermedios hasta el momento de realizar el release.

Finalmente commitear los cambios y hacer push de las ramas develop y master a origin, y luego de la tag generada también a origin.

# Sugerencia

Es posible simplificar las llamadas a dch agregando la siguiente linea a *~/.bashrc*

```bash
alias mosimpa-dch='DEBEMAIL=perezmeyer@gmail.com DEBFULLNAME="A MoSimPa developer" dch'
```

De esta manera se puede hacer llamadas del estilo:

```bash
$ mosimpa-dch -r
```