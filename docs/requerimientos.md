# Requerimientos del sistema - objetivo

**Atención:** todo lo que se encuentre <s>tachado</s> es contenido que fué volcado al [repositorio de documentación](https://gitlab.com/mosimpa/documentation).

Se proponen a continuación las especificaciones generales de un sistema que monitoree y transmita un juego mínimo de variables, suplantando en parte la necesidad de un monitor de cabecera de alta complejidad y costo. Esto permitirá disponer en un ámbito o sala creada Ad Hoc para el cuidado de pacientes (carpa sanitaria, hotel, salón, hospital de campaña) de un dispositivo para monitorear un gran número de pacientes con poco personal tal como se espera en esta situación. Se utilizarán redes WiFi para la vinculación de datos.

Este equipamiento facilitara a los actores del sistema sanitario atender múltiples pacientes con más eficacia y a su vez a los pacientes que ingresen a estos ámbitos, tener la certeza de estar siendo vigilados.

# Requerimientos generales

<s>
## Variables a monitorear

Para la primera etapa:

* Saturación de O₂.
* Frecuencia cardíaca (a partir del sensor de saturación de O₂).
* Temperatura corporal.
</s>

<a name="req_generales_dispositivo"></a>
## Dispositivos

Se propone un dispositivo:

<s>

- De uso individual.
- Alimentado a baterías para reducir el riesgo eléctrico.
- Adosable a la persona.
- Capaz de transmitir datos del paciente y estado del dispositivo por WiFi a un servidor.
- Identificado con una etiqueta con los datos del equipo en forma escrita y en un [código QR](codigos_qr).
- LEDs de estado. [Funcionalidad a definir](https://gitlab.com/mosimpa/esp32-firmware/-/issues/8).

</s>

Los dispositivos serán implementados con la tecnología Wereable Health de [Maxim](https://www.maximintegrated.com) en dos subsistemas que sientan las bases para definir los [tipos de datos a transmitir](datos_a_transmitir.md).

### Subsistema saturación de O₂ y ritmo cardíaco

La medición de saturación de O₂ y ritmo cardíaco se realiza a través de un [MAX32664A](max32664a.md) conectado a un MAX30101.

### Subsistema de temperatura corporal

Basado en el MAX30205MTA.

Los rangos útiles de cada tipo de dato se definen también en [datos a transmitir](datos_a_transmitir.md).

### Tasa de transmisión de datos

Se sugirió registrar los valores cada 30 segundos, pero hay que [hacer el análisis de que se necesita realmente](https://gitlab.com/mosimpa/esp32-firmware/-/issues/7).

## Manejo de los datos

### Servidor

<s>

Los datos recabados por los dispositivos serán concentrados en un servidor. El mismo deberá poder:

- Guardar los datos por un tiempo predeterminado.
- Manejar la información necesaria de:
    - Dispositivos.
    - Ubicaciones (camas, domicilios, etc).
    - Datos de los pacientes.
    - Internaciones. Las mismas asocian paciente, ubicación y dispositivo.
- Poder proveer los datos a aplicaciones de monitoreo (monitores).

</s>

<a name="req_generales_monitores"></a>
### Monitores

<s>

Aplicaciones capaces de mostrar los datos de los pacientes. Debe poder:

- Mostrar alarmas individuales.
- Mostrar gráficos con datos históricos y actuales para cada internación.
- [Disparar alarmas sonoras y visuales con ACK individual](https://gitlab.com/mosimpa/monitor/-/issues/8).

Existen dos formas principales:

- Una aplicación que corre en el servidor. Debe poder:
    - Mostrar los valores de cada internación en una única pantalla de manera de permitir el monitoreo múltiple de pacientes.
    - [(Opcional) Generar un reporte en PDF](https://gitlab.com/mosimpa/monitor/-/issues/7).
    - [Posibilidad de ordenar el listado de pacientes de manera que aquellos que tengan disparada una alarma queden arriba](https://gitlab.com/mosimpa/monitor/-/issues/9).
    - Generar un [código QR para consumo de los monitores móviles](codigos_qr.md#qr_en_monitor).

- Una aplicación que corre en dispositivos móviles. Debe poder:
    - Escanear el código QR del dispositivo para poder identificar rápidamente el mismo.
    - Escanear un código QR [generado por el monitor principal para poder visualizar rápidamente los datos de una internación en particular](codigos_qr.md#qr_en_monitor).

</s>

### Sistema de Alta, Baja y Modificación (ABM)

<s>

Es el sistema utilizado para dar de alta/modificar/dar de baja dispositivos, ubicaciones, pacientes e internaciones.

</s>

# Rangos de los valores medidos

Para entender bien la documentación de los tipos de [datos a transmitir](datos_a_transmitir.md) es necesario primero entender tres tipos distintos de rangos de valores:

## Rango de valores que puede emitir el sensor

Los sensores emiten sus valores utilizando una representación digital limitada por el número de bits de la misma y el funcionamiento interno del integrado.

Por ejemplo el sensor de temperatura MAX30205 representa a la misma con un entero de 16 bits complemento a dos (con signo) donde el bit menos significativo representa a 0.00390625ºC. Los valores posibles de un entero de 16 bits complemento a dos van de -32768 a 32767, representando temperaturas de -128 a 127.99 ºC. La hoja de datos del sensor estipula que el mismo opera entre 0 y 50ºC, pero no habla del comportamiento fuera de ese rango.

## Rango de valores aceptables por el sistema

<s>

El sistema debe limitar los rangos de los valores recibidos a valores plausibles de medir. Estos rangos **deben** incluir los valores esperados en el tratamiento de un paciente pero pueden ser aún mas abarcativos, de manera de poder detectar el mal funcionamiento del sistema, en particular de algún sensor.

Los mismos son:

- SpO₂ (saturación de O₂): Entre 0 y 100% (0 y 1000 en la representación otorgada por el sensor). Valores fuera de este rango no tienen sentido físico.
- Ritmo cardíaco:
    - El mínimo es 0 ya que no tiene sentido físico un valor negativo de latidos por minuto.
    - El máximo es 300 latidos por minuto, límite superior observado en pacientes con taquicardia supraventricular.
- Temperatura corporal: mínimo 0 ºC y máximo 50 ºC, siguiendo los lineamientos del fabricante.

</s>

Y se definen en el [encabezado de definiciones comunes](https://gitlab.com/mosimpa/mosimpaqt/-/blob/master/src/definitions.h) con el fin de que cada código que parsee datos verifique el rango y descarte valores por fuera de estos. La base de datos [debe también cumplimentar este requisito](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_tables.sql#L84), existiendo un chequeo de que [tanto el código como la base de datos hagan uso de los mismos valores](https://gitlab.com/mosimpa/datakeeper/-/blob/master/tests/db_tst.h#L26).

## Rango de valores de las alarmas

<s>

Los rangos de los valores de alarma permiten limitar la configuración de las mismas a valores con sentido terapéutico. A modo de contraejemplo no tiene sentido poner una alarma de baja temperatura corporal para que dispare solo cuando el valor es menor a 10 ºC, ya que esto sería un riesgo para el paciente.

Para poder evitar tener que reemplazar las implementaciones de los monitores en caso de tener que cambiar un rango los mismos serán [provistos de forma centralizada](datos_a_transmitir.md#comando-alarms_ranges).

Las alarmas con sus rangos se definen como:

- SpO₂ igual o menor a: entre [84](https://gitlab.com/mosimpa/mosimpa.gitlab.io/-/issues/5) y 95%.
- Ritmo cardíaco igual o menor a: entre 40 y 60 latidos por minuto.
- Ritmo cardíaco igual o mayor a: entre 100 y 140 latidos por minuto.
- Temperatura corporal igual o menor a: entre 35 y 37 ºC.
- Temperatura corporal igual o mayor a: entre 38 y 40 ºC.

</s>

Actualmente implementado en [datakeeper](https://gitlab.com/mosimpa/datakeeper/-/blob/master/src/settings.h) y chequeado en [su test](https://gitlab.com/mosimpa/datakeeper/-/blob/master/tests/settingstests.h#L12).

<s>

A su vez cada tipo de alarma poseerá un retraso configurable de 15 a 60 segundos, valores recomendados en el estudio [Retrospective analysis of pulse oximeter alarm settings in an intensive care unit patient population](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4891882/).

</s>

## Valores por omisión de las alarmas

<s>

Al crear una internación se usan los siguientes valores por omisión:

- SpO₂: 95%.
- Ritmo cardíaco igual o menor a: 60 latidos por minuto.
- Ritmo cardíaco igual o mayor a: 100 latidos por minuto.
- Temperatura corporal igual o menor a: 37 ºC.
- Temperatura corporal igual o mayor a: 38 ºC.
- Delay de cada alarma: 15".

</s>

Se implementa [sobre la base de datos](https://gitlab.com/mosimpa/datakeeper/-/blob/master/sql/create_tables.sql#L62) al crear una nueva internación.

# Requerimientos del software

Sección 5.2 IEC62304.

<s>

## Requerimientos generales de software

Todas las transmisiones de datos que entren o salgan del servidor deben ser encriptadas. Aquellas que sean intra-servidor (datakeeper con la base de datos o monitor general con datakeeper) no necesitan serlo.

</s>

## Particionado del software

![Particionado básico del software](requerimientos/mosimpa_particionado.png)

[Fuente](requerimientos/mosimpa_particionado.dia). El esquema representa el particionado básico del sistema.

## Dispositivo

Debido a que la funcionalidad y el riesgo del dispositivo es inherente (pero no exclusivo) al firmware del mismo se clasifica como como categoría B: existe la posibilidad de lesiones no serias.

<s>

Debe implementar la funcionalidad requerida por los [requerimientos generales](#req_generales_dispositivo) del mismo.

</s>

Debe obtener los datos de los sensores y transmitirlos siguiendo el [formato de los datos especificado](datos_a_transmitir.md).

<s>

Debe poder ser configurable para asociarlo a un servidor MQTT y una red WiFi específica.

Debe poder transmitir todos sus datos de forma encriptada usando TLS.

Debe poder obtener la tensión de batería y transmitirla al sistema.

</s>

## ABM

<s>

La aplicación de **A**lta, **B**aja y **M**odificación permite la entrada en el sistema de equipos, ubicaciones, pacientes e internaciones, comunicándose directamente con la base de datos.

Se debe tratar de establecer un procedimiento que reduzca el riesgo de asociar un paciente a un equipo erróneo. Por este motivo se lo clasifica como categoría B.

Esta aplicación se corre desde el mismo servidor que la base de datos y datakeeper, por lo que no es necesario encriptar la comunicación.

Es necesario crear [documentación de uso para el usuario](https://gitlab.com/mosimpa/abm/-/issues/8).

## Datakeeper

Tiene la misión de:

- Recibir los datos de los dispositivos, verificarlos y de ser correctos ingresarlos en la base de datos.
- Recibir y reponder consultas de los monitores según lo especificado en [datos a transmitir](datos_a_transmitir).

</s>

Es de suma importancia que el software no altere los datos recibidos y enviados, por lo que se lo clasifica como categoría B.

Esta aplicación se conecta tanto al broker MQTT como a la base de datos, todos dentro del servidor, por lo que no es necesario encriptar estas comunicaciones.

<s>

## Monitor general

Debe cumplimentar con [las especificaciones de un monitor general](#req_generales_monitores). El flujo de datos debe garantizar que los mismos no sean alterados, por lo que se cataloga como categoría B.

Se conecta al broker MQTT, ambos corriendo en el servidor, por lo que no es necesario encriptar los datos.

</s>

## Monitor móvil

Debe cumplimentar con [las especificaciones de un monitor general](#req_generales_monitores). El flujo de datos debe garantizar que los mismos no sean alterados, por lo que se cataloga como categoría B.

Se conecta al broker MQTT de forma externa al servidor, por lo que [es obligatorio encriptar los datos](https://gitlab.com/mosimpa/mosimpa-android/-/issues/2).

Es necesario crear [documentación de uso para el usuario](https://gitlab.com/mosimpa/mosimpa-android/-/issues/1).

<s>

## Base de datos

Elemento SOUP (Software Of Unknown Provenance, IEC 62304 seccción 3.29).

Se utiliza [PostgreSql](https://www.postgresql.org/) por implementar el estándar SQL, ser ACID-compliant, open source y disponible en el sistema operativo elegido.

Los clientes que se conectan a la base de datos son todos locales al servidor.

## Broker MQTT

Elemento SOUP (Software Of Unknown Provenance, IEC 62304 seccción 3.29).

Se utiliza [Mosquitto](https://mosquitto.org/) por ser una implementación open source probada de un broker MQTT, estar disponible en el sistema operativo elegido y proveer bibliotecas en C para el desarrollo de las aplicaciones.

Los dispositivos y monitores externos se conectan al mismo desde fuera del servidor, por lo que es necesario que pueda establecerse una conexión encriptada.

</s>

# Arquitectura general

La arquitectura general del sistema, desde el punto de vista del software, se describe mejor con la siguiente imagen:

![Esquema de datos](requerimientos/esquema_conexiones.png)

Esquema de aplicaciones y conexiones, detallando cuales **deben** ser encriptadas. [Fuente](requerimientos/esquema_conexiones.odg).

## Dispositivo

En la siguiente imagen se describe la arquitectura general del firmware implementado en el device:

![Esquema de datos](images/device/block_diagram_basic_arch.png)

## Datakeeper - unidades de software

![Unidades de software datakeeper](requerimientos/datakeeper_particionado.png)

[Fuente](requerimientos/datakeeper_particionado.dia).

## ABM - Unidades de software

En este punto tenemos que decidir bien si es categoría A o B. El único riesgo encontrado hasta el momento es metodológico del usuario para asociar un paciente a un dispositivo.

## Monitor general

![Unidades de software monitor](requerimientos/monitor_particionado.png)

[Fuente](requerimientos/monitor_particionado.dia).

## Monitor móvil

En la siguiente imagen se describe la arquitectura general del software implementado en el monitor móvil:

![Esquema de datos](images/mobile/block_diagram_architecture.png)

