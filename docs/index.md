# Monitoreo Simplificado de Pacientes en situación de internación masiva - MoSimPa

![Para Ayudar](images/paraayudar.png)

## En las redes

 - [Instagram](https://www.instagram.com/paraayudardar/)
 - [Facebook](https://www.facebook.com/paraayudardar/)

## En las noticias

- [Software libre en dispositivos médicos. El caso del proyecto "Monitoreo simplificado de pacientes en situación de internación masiva (MoSimPa)"](https://debconf20.debconf.org/talks/96-software-libre-en-dispositivos-medicos-el-caso-del-proyecto-monitoreo-simplificado-de-pacientes-en-situacion-de-internacion-masiva-mosimpa/) - DebConf20, the annual Debian connference. Videos:
    - [Calidad original](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/96-software-libre-en-dispositivos-medicos-el-caso-del-proyecto-monitoreo-simplificado-de-pacientes-en-situacion-de-internacion-masiva-mosimpa.webm), 143 MB.
    - [Baja calidad](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/96-software-libre-en-dispositivos-medicos-el-caso-del-proyecto-monitoreo-simplificado-de-pacientes-en-situacion-de-internacion-masiva-mosimpa.lq.webm), 84 MB.
- [Bahía Blanca, clave en la fabricación de un equipo para evitar contagios de coronavirus en hospitales](https://www.lanueva.com/nota/2020-5-15-6-30-27-bahia-clave-en-la-fabricacion-de-un-equipo-para-evitar-contagios-en-hospitales) - LaNueva.com 15/05/2020
- [El ingenioso desarrollo argentino para evitar que médicos y enfermeros se contagien de coronavirus y supervisen pacientes a distancia](https://www.infobae.com/coronavirus/2020/05/14/el-ingenioso-desarrollo-argentino-para-evitar-que-medicos-y-enfermeros-se-contagien-de-coronavirus-y-supervisen-pacientes-a-distancia/) - Infobae 14/05/2020.
- [Fuerte inversión de Nación en los proyectos de dos investigadores bahienses sobre COVID-19](https://www.lanueva.com/nota/2020-5-4-6-30-15-fuerte-inversion-de-nacion-en-los-proyectos-de-dos-investigadores-bahienses-sobre-covid-19) - LaNueva.com 4/05/2020.
- [Quienes son los 64 científicos argentinos elegidos para financiar sus investigaciones sobre Covid-19](https://www.infobae.com/coronavirus/2020/05/02/quienes-son-los-64-cientificos-argentinos-elegidos-para-financiar-sus-investigaciones-sobre-covid-19/) Infobae 02/05/2020.
- [Desarrollan dispositivo inalámbrico para pacientes con COVID](https://www.youtube.com/watch?v=tYHs-mXgaCg) - Youtube Canal Siete BB 23/04/2020.
- [Diseñan un dispositivo de monitoreo inalámbrico para pacientes](https://www.facebook.com/notes/conicet-bah%C3%ADa-blanca/dise%C3%B1an-un-dispositivo-de-monitoreo-inal%C3%A1mbrico-para-pacientes/2533881213543612/) - Facebook CONICET Bahía Blanca 22/04/2020.

# General

El código fuente desarrollado para este proyecto se encuentra en el [repositorio de Gitlab](https://gitlab.com/mosimpa/).

Las propuestas analizadas para el desarrollo se pueden ver en [este link](propuesta.md).

La documentación del repositorio [documentation](https://gitlab.com/mosimpa/documentation) se puede ver renderizada [aquí](https://mosimpa.gitlab.io/documentation/).

# Requerimientos, arquitectura

- [Requerimientos del sistema](requerimientos.md)
- [Datos a transmitir](datos_a_transmitir.md).
- [Códigos QR](codigos_qr.md).
- [Flujo de programa de software de test rig](firmware/puesta_en_marcha.md).


# Guías y procedimientos

- [Generación de la imagen del SO](imagen_so/index.md).
- [Proceso de release de software para repositorios usando git flow](git_flow/release.md).

# Datos técnicos

- [Presión sanguínea, ritmo cardíaco, SpO2: MAX32664A](max32664a.md).

# Manuales de usuario

Se pueden encontrar renderizados [aquí](https://mosimpa.gitlab.io/ProductDocumentation/ "Documentación del producto").
